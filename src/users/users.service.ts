import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { LoginUserDto, LoginUserRo } from './dto/login-user.dto.';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User) private usersRepository: Repository<User>,
  ) {}

  async create(createUserDto: CreateUserDto) {
    const { username } = createUserDto;
    let user = await this.usersRepository.findOne({ where: { username } });

    if (user) {
      throw new HttpException('User already exist', HttpStatus.BAD_REQUEST);
    }

    user = await this.usersRepository.create(createUserDto);
    await this.usersRepository.save(user);

    return user.toResponseObject();
  }

  async findAll(page = 1): Promise<LoginUserRo[]> {
    const users = await this.usersRepository.find({
      relations: ['jobs', 'bookmarks'],
      skip: 2 * (page - 1),
      take: 2,
    });
    if (!users) {
      throw new HttpException('Not found', HttpStatus.NOT_FOUND);
    }
    return users.map((data) => data.toResponseObject(false));
  }

  async login(loginUserDto: LoginUserDto) {
    const { username, password } = loginUserDto;
    const user = await this.usersRepository.findOne({ where: { username } });

    if (!user || !(await user.comparePassword(password))) {
      throw new HttpException(
        'Invalid username/password',
        HttpStatus.BAD_REQUEST,
      );
    }

    return user.toResponseObject();
  }

  async findOne(id: string) {
    const user = await this.usersRepository.findOne({
      where: { id },
      relations: ['jobs'],
    });

    if (!user) {
      throw new HttpException('Not Found', HttpStatus.NOT_FOUND);
    }

    return user;
  }

  async update(id: string, updateUserDto: Partial<UpdateUserDto>) {
    let user = await this.usersRepository.findOne({ where: { id } });

    if (!user) {
      throw new HttpException('Not found', HttpStatus.NOT_FOUND);
    }
    await this.usersRepository.update({ id }, updateUserDto);
    user = await this.usersRepository.findOne({ where: { id } });
    return user;
  }

  async remove(id: string) {
    const user = await this.usersRepository.findOne({ where: { id } });
    if (!user) {
      throw new HttpException('Not found', HttpStatus.NOT_FOUND);
    }

    await this.usersRepository.delete({ id });
    return { deleted: true };
  }
}
