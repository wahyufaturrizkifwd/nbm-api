import { IsNotEmpty } from 'class-validator';
import { Job } from 'src/jobs/entities/job.entity';

export class LoginUserDto {
  @IsNotEmpty()
  username: string;

  @IsNotEmpty()
  password: string;
}

export class LoginUserRo {
  id: string;
  username: string;
  token?: string;
  firstName?: string;
  lastName?: string;
  email?: string;
  phoneNumber?: string;
  bookmarks?: Job[];
  isActive?: boolean;
  createdAt: Date;
  updatedAt: Date;
  deletedAt: Date;
}
