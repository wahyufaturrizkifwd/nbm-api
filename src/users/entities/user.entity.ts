import * as bcrypt from 'bcryptjs';
import { IsBoolean, IsDefined, IsString, MinLength } from 'class-validator';
import * as jwt from 'jsonwebtoken';
import { Job } from 'src/jobs/entities/job.entity';
import {
  BeforeInsert,
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { LoginUserRo } from '../dto/login-user.dto.';

@Entity()
export class User {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({
    type: 'text',
    unique: true,
  })
  username: string;

  @Column('text')
  password: string;

  @Column('text')
  @IsDefined({ always: true })
  @IsString({ always: true })
  @MinLength(2, { always: true })
  firstName: string;

  @Column('text')
  @IsDefined({ always: true })
  @IsString({ always: true })
  @MinLength(2, { always: true })
  lastName: string;

  @Column('text')
  @IsDefined({ always: true })
  @IsString({ always: true })
  @MinLength(3, { always: true })
  email: string;

  @Column('text')
  @IsDefined({ always: true })
  @IsString({ always: true })
  @MinLength(3, { always: true })
  phoneNumber: string;

  @OneToMany((type) => Job, (jobs) => jobs.user)
  jobs: Job[];

  @ManyToMany((type) => Job, { cascade: true })
  @JoinTable()
  bookmarks: Job[];

  @Column()
  @IsBoolean()
  isActive: boolean;

  @CreateDateColumn() createdAt: Date;

  @UpdateDateColumn() updatedAt: Date;

  @DeleteDateColumn() deletedAt: Date;

  @BeforeInsert()
  async hashPassword() {
    this.password = await bcrypt.hash(this.password, 10);
  }

  // eslint-disable-next-line @typescript-eslint/no-inferrable-types
  toResponseObject(showToken: boolean = true): LoginUserRo {
    const {
      id,
      username,
      token,
      firstName,
      lastName,
      email,
      phoneNumber,
      isActive,
      createdAt,
      updatedAt,
      deletedAt,
    } = this;

    const responseObject: any = {
      id,
      username,
      firstName,
      lastName,
      email,
      phoneNumber,
      isActive,
      createdAt,
      updatedAt,
      deletedAt,
    };

    if (showToken) {
      responseObject.token = token;
    }

    if (this.jobs) {
      responseObject.jobs = this.jobs;
    }

    if (this.bookmarks) {
      responseObject.bookmarks = this.bookmarks;
    }

    return responseObject;
  }

  async comparePassword(attempt: string) {
    return await bcrypt.compare(attempt, this.password);
  }

  private get token() {
    const { id, username } = this;
    return jwt.sign(
      {
        id,
        username,
      },
      process.env.SECRET,
      { expiresIn: '7d' },
    );
  }
}
