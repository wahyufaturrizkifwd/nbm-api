import { Body, Controller, Get, Param, Post, UseGuards, Headers } from '@nestjs/common';
import { ApiExtraModels, ApiResponse, ApiTags, getSchemaPath, ApiSecurity, ApiHeader } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { UserDecorator } from 'src/users/users.decorator';
import { CancelPoliciesReqDto, CancelPoliciesResDto } from './dto/cancel-policies.dto';
import { SubmitAppliactionResDto, SubmitAppliactionReqDto } from './dto/submit-applications.dto'
import { IssuePoliciesResDto, IssuePoliciesReqDto } from './dto/issue-policies.dto'
import { CalculatePremiumResDto, CalculatePremiumReqDto } from './dto/calculate-premium.dto'
import { ProductPlansDto } from './dto/product-plans.dto';
import { ProductsDto } from './dto/products.dto';
import { PartnerService } from './partner.service';

@ApiExtraModels(ProductsDto)
@ApiExtraModels(ProductPlansDto)
@ApiExtraModels(CancelPoliciesResDto)
@ApiExtraModels(SubmitAppliactionResDto)
@ApiExtraModels(SubmitAppliactionReqDto)
@ApiExtraModels(IssuePoliciesResDto)
@ApiExtraModels(CalculatePremiumResDto)

@Controller()
export class PartnerController {
  constructor(private readonly partnerService: PartnerService) { }


  @Get('v1/products')
  @ApiTags('get all products')
  @ApiResponse({
    status: 200,
    description: 'returns plans by product',
    schema: {
      $ref: getSchemaPath(ProductsDto)
    }
  })
  @ApiHeader({
    name: 'Language',
    required: false
  })
  @UseGuards(AuthGuard('api-key'))
  @ApiSecurity('apiKey')
  getAllProducts(
    @Headers('countryCode') countryCode: string,
    @Headers('partnerCode') partnerCode: string,
    @Headers('Language') Language?: string): Promise<ProductsDto> {
    return this.partnerService.getProducts();
  }

  @Get('v1/:productCode/plans')
  @ApiTags('get plans by product ')
  @ApiResponse({
    status: 200,
    description: 'returns plans by product',
    schema: {
      $ref: getSchemaPath(ProductPlansDto)
    }
  })
  @ApiHeader({
    name: 'Language',
    required: false
  })
  @UseGuards(AuthGuard('api-key'))
  @ApiSecurity('apiKey')
  getPlansByProductCode(
    @Param('productCode') productCode: string,
    @Headers('countryCode') countryCode: string,
    @Headers('partnerCode') partnerCode: string,
    @Headers('Language') Language?: string): Promise<ProductPlansDto> {
    return;
  }

  @Post('v1/submitApplications')
  @ApiTags('submit application')
  @ApiResponse({
    status: 200,
    description: 'submit application',
    schema: {
      $ref: getSchemaPath(SubmitAppliactionResDto)
    }
  })
  @ApiHeader({
    name: 'Language',
    required: false
  })
  @UseGuards(AuthGuard('api-key'))
  @ApiSecurity('apiKey')
  postPartnerAppliaction(
    @Headers('countryCode') countryCode: string,
    @Headers('partnerCode') partnerCode: string,
    @Body() submitAppliactionReqDto: SubmitAppliactionReqDto,
    @Headers('Language') Language?: string): Promise<SubmitAppliactionResDto> {
    return;
  }

  @Post('v1/calculatePremium')
  @ApiTags('calculate premium')
  @ApiResponse({
    status: 200,
    description: 'calculate the final premium base on the plan customer chooses',
    schema: {
      $ref: getSchemaPath(CalculatePremiumResDto)
    }
  })
  @ApiHeader({
    name: 'Language',
    required: false
  })
  @UseGuards(AuthGuard('api-key'))
  @ApiSecurity('apiKey')
  postCalculatePremium(
    @Headers('countryCode') countryCode: string,
    @Headers('partnerCode') partnerCode: string,
    @Body() calculatePremiumDataReqDto: CalculatePremiumReqDto,
    @Headers('Language') Language?: string): Promise<CalculatePremiumResDto> {
    return;
  }

  @Post('v1/issuePolicies')
  @ApiTags('issue policies')
  @ApiResponse({
    status: 200,
    description: 'issue policies base on the plan customer chooses',
    schema: {
      $ref: getSchemaPath(IssuePoliciesResDto)
    }
  })
  @ApiHeader({
    name: 'Language',
    required: false
  })
  @UseGuards(AuthGuard('api-key'))
  @ApiSecurity('apiKey')
  postIssuePolicies(
    @Headers('countryCode') countryCode: string,
    @Headers('partnerCode') partnerCode: string,
    @Body() issuePoliciesReqDto: IssuePoliciesReqDto,
    @Headers('Language') Language?: string): Promise<IssuePoliciesResDto> {
    return;
  }

  @Post('v1/cancelPolicies')
  @ApiTags('cancel policies')
  @ApiResponse({
    status: 200,
    description: 'cancel policies base on the plan customer chooses',
    schema: {
      $ref: getSchemaPath(CancelPoliciesResDto)
    }
  })
  @ApiHeader({
    name: 'Language',
    required: false
  })
  @UseGuards(AuthGuard('api-key'))
  @ApiSecurity('apiKey')
  postCancelPolicies(
    @Headers('countryCode') countryCode: string,
    @Headers('partnerCode') partnerCode: string,
    @Body() cancelPoliciesReqDto: CancelPoliciesReqDto,
    @Headers('Language') Language?: string): Promise<CancelPoliciesResDto> {
    return;
  }

}
