import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/users/entities/user.entity';
import { Repository } from 'typeorm';
import { CreatePartnerDto } from './dto/create-partner.dto';
import { UpdatePartnerDto } from './dto/update-partner.dto';
import { ProductPlansDto } from './dto/product-plans.dto';
import { ProductsDto } from './dto/products.dto'
import { SubmitAppliactionResDto } from './dto/submit-applications.dto';

@Injectable()
export class PartnerService {
  constructor(
    @InjectRepository(User) private userRepository: Repository<User>,
  ) { }

  create(createPartnerDto: CreatePartnerDto) {
    return 'This action adds a new partner';
  }

  findAll() {
    return `This action returns all partner`;
  }

  findOne(id: number) {
    return `This action returns a #${id} partner`;
  }

  update(id: number, updatePartnerDto: UpdatePartnerDto) {
    return `This action updates a #${id} partner`;
  }

  remove(id: number) {
    return `This action removes a #${id} partner`;
  }

  async getProducts() {
    return {} as ProductsDto
  }
  async getPlansByProductCode(id: string, userId: string) {
    const user = await this.userRepository.findOne({
      where: { id: userId },
    });

    if (user) {
      return {
        "status": "success",
        "data": {
          "product": {
            "productCode": "mpa",
            "productName": "Micro PA",
            "productDesc": "",
            "plan": [
              {
                "planCode": "mpaNormal",
                "planName": "60K Plan - Normal Activities",
                "premiumType": "Single",
                "benefits": [],
                "riders": [],
                "currency": "SGD",
                "coverage": {
                  "type": "day",
                  "sumAssured": "60000"
                },
                "validation": {
                  "0": {
                    "type": "age",
                    "rules": {
                      "ageRangeFrom": "15",
                      "ageRangeFromUnit": "day",
                      "ageRangeTo": "75",
                      "ageRangeToUnit": "year"
                    }
                  },
                  "1": {
                    "type": "insured",
                    "rules": {
                      "maxInsuredList": "unlimited"
                    }
                  },
                  "2": {
                    "type": "coverageDuration",
                    "rules": {
                      "unit": "day",
                      "minDuration": "1",
                      "maxDuration": "365"
                    }
                  }
                },
                "uw": [
                  {
                    "uwQnId": "uwQn1",
                    "uwQnLabel": "Are you smoking",
                    "uwQnValue": "Yes"
                  }
                ]
              }
            ],
            "declaration": [
              {
                "declarationQnId": "decQn1",
                "declarationQnLabel": "Declaration 1",
                "declarationQnValue": "Yes"
              }
            ]
          }
        }
      } as unknown as ProductPlansDto;
    }

  }

  async postPartnerAppliaction(id: string, userId: string) {
    const user = await this.userRepository.findOne({
      where: { id: userId },
    });

    if (user) {
      return {
        "status": "success",
        "data": {
          "applicationId": "R07Y307701I9",
          "policyId": "",
          "product": {
            "productCode": "mpa",
            "plan": {
              "planCode": "mpaNormal",
              "riders": [],
              "coverage": {
                "sumAssured": "60,000",
                "coverageStart": "011221",
                "coverageEnd": "201221"
              }
            }
          }
        }
      } as unknown as SubmitAppliactionResDto;
    }

  }
}
