import { ApiProperty } from '@nestjs/swagger';
import {
    ApiResponsesDto, ProductItemDto, OthersDto, discountsItemDto, PremiumsItemDto, DeclarationDto
    , PolicyHolderDto, InsuredDto, RelationDto, PolicyExtraInfoDto
} from './common.dto';

import { policyListExample, issuePoliciesDataResExample } from '../../examples/issue-poicies'
export class OthersDataDto {
    @ApiProperty({
        description: 'beneficiary',

    })
    type: String;

    @ApiProperty({
        description: 'firstName'

    })
    firstName: String;

    @ApiProperty({
        description: 'lastName'

    })
    lastName: String;

    @ApiProperty({
        description: 'extraInfo'

    })
    extraInfo: String;

    @ApiProperty({
        description: 'proportion',
        example: '100%'

    })
    proportion: String;

    @ApiProperty({
        description: 'relation'

    })
    relation: RelationDto;
}

export class ReqRolesItemDto {
    @ApiProperty({
        description: 'policyHolder',

    })
    policyHolder: PolicyHolderDto;

    @ApiProperty({
        description: 'insured',
        type: [InsuredDto]

    })
    insured: [InsuredDto];

    @ApiProperty({
        description: 'others',
        type: [OthersDataDto]

    })
    others: [OthersDataDto];
}

export class IssuePoliciesDataReqDto {
    @ApiProperty({
        description: 'application Id',
        example: 'S32423423G'
    })
    applicationId: string

    @ApiProperty({
        description: 'policy Id',
        example: 'MPA00001'
    })
    policyId: string

    @ApiProperty({
        description: 'product'

    })
    product: ProductItemDto;

    @ApiProperty({
        description: 'roles'

    })
    roles: ReqRolesItemDto;

    @ApiProperty({
        description: 'declaration',
        type: [DeclarationDto],
        minimum: 1,
        maximum: 1
    })
    declaration: [DeclarationDto];

    @ApiProperty({
        description: 'premiums'
    })
    premiums: PremiumsItemDto;

    @ApiProperty({
        description: 'discounts'

    })
    discounts: discountsItemDto;

    @ApiProperty({
        description: 'extra info'
    })
    extraInfo: PolicyExtraInfoDto

    @ApiProperty({
        description: 'others'

    })
    others: OthersDto;

}


export class IssuePoliciesReqDto {

    @ApiProperty({
        description: 'request submit application data',
        example: 1
    })
    policyTotal: number

    @ApiProperty({
        description: 'request issue policies Data',
        example: policyListExample
    })
    policyList: [IssuePoliciesDataReqDto];
}


export class IssuePoliciesDataResDto {
    @ApiProperty({
        description: 'applicationId',
        example: 'R07Y307701I9'
    })
    applicationId: String;

    @ApiProperty({
        description: 'policyId',
        example: 'MPA00001'
    })
    policyId: String;

    @ApiProperty({
        description: 'product',
    })
    product: ProductItemDto;
}


export class IssuePoliciesResDto extends ApiResponsesDto {
    @ApiProperty({
        description: 'Response Data',
        example: issuePoliciesDataResExample
    })
    data: IssuePoliciesDataResDto;
}