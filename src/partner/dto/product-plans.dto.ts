import { ApiProperty } from '@nestjs/swagger';
import { ApiResponsesDto } from './common.dto';
import { RuleAgeDto } from './rules/rule-age.dto';
import { RuleCoverageDurationDto } from './rules/rule-coverage-duration.dto';
import { RuleInsuredListDto } from './rules/rule-insured-list.dto';
import { DeclarationDto } from './common.dto';
import { CoverageDto } from './common.dto';
import { UwDto } from './common.dto';
import { productPlansDataResExample } from '../../examples/product-plans'



export class validationDto {
    @ApiProperty({
        description: 'Age Rule'
    })
    0: RuleAgeDto;

    @ApiProperty({
        description: 'Insured list rule'
    })
    1: RuleInsuredListDto;

    @ApiProperty({
        description: 'Coverage duration rule'
    })
    2: RuleCoverageDurationDto;
}

export class ProductPlanDto {
    @ApiProperty({
        description: 'Plan code',
        example: 'mpaNormal'
    })
    planCode: String;

    @ApiProperty({
        description: 'Plan name',
        example: '60K Plan - Normal Activities'
    })
    planName: String;

    @ApiProperty({
        description: 'Product Premium (Single Premium; Renewable)',
        example: 'single'
    })
    premiumType: String;

    @ApiProperty({
        description: 'Product benefits',
        example: []
    })
    benefits: string[];

    @ApiProperty({
        description: 'Product riders',
        example: []
    })
    riders: string[];

    @ApiProperty({
        description: 'Product riders',
        example: 'SGD'
    })
    currency: string;

    @ApiProperty({
        description: 'Coverage',
        example: 'SGD'
    })
    coverage: CoverageDto;

    @ApiProperty({
        description: 'Product validation'
    })
    validation: validationDto;

    @ApiProperty({
        description: 'Product uw',
        type: [UwDto]
    })
    uw: [UwDto];


}

export class ProductPlansDataDto {
    @ApiProperty({
        description: 'Product code',
        example: 'MPA'
    })
    productCode: String;

    @ApiProperty({
        description: 'Micro PA',
        example: 'Micro pa'
    })
    productName: String;

    @ApiProperty({
        description: 'Product description',
        example: 'Micro PA'
    })
    productDescription: String;

    @ApiProperty({
        description: 'Product plan description',
        type: [ProductPlanDto]
    })
    plan: [ProductPlanDto];

    @ApiProperty({
        description: 'Declaration',
        type: [DeclarationDto]
    })
    declaration: [DeclarationDto];
}


export class ProductPlansDto extends ApiResponsesDto {
    @ApiProperty({
        description: 'Response Data',
        example: {
            product: productPlansDataResExample
        }
    })
    data: {
        product: ProductPlansDataDto
    };
}
