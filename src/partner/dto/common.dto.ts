import { ApiProperty } from '@nestjs/swagger';

export class ApiResponsesDto {
    @ApiProperty({
        description: 'Responses status',
        example: 'success'
    })
    status: string;
}

export class CoverageDto {
    @ApiProperty({
        description: 'Coverage sum assured',
        example: 'day'
    })
    type?: string;

    @ApiProperty({
        description: 'Coverage sum assured',
        example: "60000"
    })
    sumAssured: string;

    @ApiProperty({
        description: 'Coverage start date',
        example: '011221'
    })
    coverageStart: string;

    @ApiProperty({
        description: 'Coverage start date',
        example: '201221'
    })
    coverageEnd: string;
}
export class planItemDto {
    @ApiProperty({
        description: 'planCode',
        example: 'mpaNormal'
    })
    planCode: String;

    @ApiProperty({
        description: 'product',
        example: []
    })
    riders: string[];

    @ApiProperty({
        description: 'coverage',
    })
    coverage: CoverageDto;
}

export class ProductItemDto {
    @ApiProperty({
        description: 'productCode',
        example: 'mpa'
    })
    productCode: String;

    @ApiProperty({
        description: 'product',
    })
    plan: planItemDto;
}
export class DeclarationDto {
    @ApiProperty({
        description: 'declarationQnId',
        example: 'decQn1'
    })
    declarationQnId: string;

    @ApiProperty({
        description: 'declarationQnLabel',
        example: 'Declaration 1'
    })
    declarationQnLabel: string;

    @ApiProperty({
        description: 'declarationQnValue',
        example: true
    })
    declarationQnValue: boolean;

    @ApiProperty({
        description: 'declarationQnAns',
        example: 'yes'
    })
    declarationQnAns?: string;
}


export class UwDto {
    @ApiProperty({
        description: 'UwQnId',
        example: 'uwQn1'
    })
    uwQnId: string;

    @ApiProperty({
        description: 'uwQnLabel',
        example: 'Are you smoking'
    })
    uwQnLabel: string;

    @ApiProperty({
        description: 'uwQnValue',
        example: true
    })
    uwQnValue: boolean;

    @ApiProperty({
        description: 'uwQnAns',
        example: 'yes'
    })
    uwQnAns?: string;
}


export class OthersDto {
    @ApiProperty({
        description: 'utm campaign',
        example: ''
    })
    utmCampaign: string;

    @ApiProperty({
        description: 'utm content',
        example: ''
    })
    utmContent: string;

    @ApiProperty({
        description: 'utm medium',
        example: ''
    })
    utmMedium: string;

    @ApiProperty({
        description: 'utm source',
        example: ''
    })
    utmSource: string;

    @ApiProperty({
        description: 'utm term',
        example: ''
    })
    utmTerm: string;

    @ApiProperty({
        description: 'partner txId',
        example: ''
    })
    partnerTxId: string;

    @ApiProperty({
        description: 'partner code',
        example: ''
    })
    partnerCode: string;
}


export class T1Dto {
    @ApiProperty({
        description: 'frontendCode',
        example: 'THVO2'
    })
    frontendCode: String;

    @ApiProperty({
        description: 'frontendLabel',
        example: 'Enjoy THVO2'
    })
    frontendLabel: String;

    @ApiProperty({
        description: 'discountTierType',
        example: 'Marketing Promo'
    })
    discountTierType: String;

    @ApiProperty({
        description: 'discountType',
        example: 'PERCENTAGE'
    })
    discountType: String;

    @ApiProperty({
        description: 'discountValue',
        example: 0
    })
    discountValue: number;

    @ApiProperty({
        description: 'voucherValue',
        example: 2000
    })
    voucherValue: number;

    @ApiProperty({
        description: 'voucherLabel',
        example: 'starbucks voucher'
    })
    voucherLabel: String;

    @ApiProperty({
        description: 'genericVoucher',
        example: false
    })
    genericVoucher: boolean;

    @ApiProperty({
        description: 'corePromoValue',
        example: 0
    })
    corePromoValue: number;

    @ApiProperty({
        description: 'corePromoCode',
        example: 'PR0000'
    })
    corePromoCode: String;

    @ApiProperty({
        description: 'additionalVoucherDiscount',
        example: []
    })
    additionalVoucherDiscount: String[];

    @ApiProperty({
        description: 'finalPromoHash',
        example: ''
    })
    finalPromoHash: String;

    @ApiProperty({
        description: 'finalPromoRand',
        example: ''
    })
    finalPromoRand: String;
}

export class LayeredPromoCodeDto {
    @ApiProperty({
        description: 't1',
        type: [T1Dto],
        minItems: 1,
        maxItems: 1
    })
    T1: [T1Dto];

    @ApiProperty({
        description: 't2',
        type: [T1Dto],
        minItems: 1,
        maxItems: 1
    })
    T2: [T1Dto];

    @ApiProperty({
        description: 't3',
        type: [T1Dto],
        minItems: 1,
        maxItems: 1
    })
    T3: [T1Dto];
}


export class discountsItemDto {
    @ApiProperty({
        description: 'product',
        example: 'T08A'
    })
    product: String;

    @ApiProperty({
        description: 'promoCode',
        example: 'PR0000'
    })
    promoCode: String;

    @ApiProperty({
        description: 'promoCodeAmount',
        example: 'AMT000'
    })
    promoCodeAmount: String;

    @ApiProperty({
        description: 'promoCodeTierType',
        example: 'Marketing Promo'
    })
    promoCodeTierType: String;

    @ApiProperty({
        description: 'frontendPromoCode',
        example: 'THVO2'
    })
    frontendPromoCode: String;

    @ApiProperty({
        description: 'lifePromoCode',
        example: ''
    })
    lifePromoCode: String;

    @ApiProperty({
        description: 'lifePromoCodeAmount',
        example: ''
    })
    lifePromoCodeAmount: String;

    @ApiProperty({
        description: 'promoCodeDesc',
        example: ''
    })
    promoCodeDesc: String;

    @ApiProperty({
        description: 'promoPercentValue',
        example: 0
    })
    promoPercentValue: number;

    @ApiProperty({
        description: 'promoAmountValue',
        example: 0
    })
    promoAmountValue: number;

    @ApiProperty({
        description: 'allPromoSame',
        example: true
    })
    allPromoSame: Boolean;

    @ApiProperty({
        description: 'layeredPromoCode'
    })
    layeredPromoCode: LayeredPromoCodeDto;

    @ApiProperty({
        description: 'promoCodeHash',
        example: 'cc8e0cb5a6923168c6d2b0f3f27a3b1537305af303b67a333dc461d4db22da51'
    })
    promoCodeHash: String;

    @ApiProperty({
        description: 'promoCodeRand',
        example: 'c701bf4c74f5722ff30bbb9235ffe7d2'
    })
    promoCodeRand: String;

    @ApiProperty({
        description: 'startDate',
        example: '2021-07-03 00:00:00'
    })
    startDate: string;

    @ApiProperty({
        description: 'expiryDate',
        example: '2022-08-03 23:59:00'
    })
    expiryDate: string;

    @ApiProperty({
        description: 'additionalQuestions',
        example: []
    })
    additionalQuestions: string[];
}



export class PremiumTaxDto {
    @ApiProperty({
        description: 'premium',
        example: 1193
    })
    premium: number;
}

export class DiscountDto {
    @ApiProperty({
        description: 'beforeTax'
    })
    beforeTax: PremiumTaxDto;

    @ApiProperty({
        description: 'afterTax'
    })
    afterTax: PremiumTaxDto;
}

export class PremiumsItemDto {
    @ApiProperty({
        description: 'currency',
        example: 'SGD'
    })
    currency: String;

    @ApiProperty({
        description: 'tax',
        example: 'SGD'
    })
    tax: String;

    @ApiProperty({
        description: 'before discount'
    })
    beforeDiscount: DiscountDto;

    @ApiProperty({
        description: 'after discount',
        example: {
            "beforeTax": {
                "premium": 1093
            },
            "afterTax": {
                "premium": 1093
            }
        }
    })
    afterDiscount: DiscountDto;
}



export class IdDto {
    @ApiProperty({
        description: 'idType',
        example: 'PASSPORT'

    })
    idType: String;

    @ApiProperty({
        description: 'idNo',
        example: 'PASSPORT'

    })
    idNo: String;
}

export class AddressDto {
    @ApiProperty({
        description: 'addressType',
        example: 'personal'

    })
    addressType: String;

    @ApiProperty({
        description: 'addressType',
        example: 'line'

    })
    addressStyle: String;

    @ApiProperty({
        description: 'addressLine1',
        example: ''
    })
    addressLine1: String;

    @ApiProperty({
        description: 'addressLine2',
        example: ''

    })
    addressLine2: String;

    @ApiProperty({
        description: 'addressLine3',
        example: ''

    })
    addressLine3: String;

    @ApiProperty({
        description: 'addressLine4',
        example: ''

    })
    addressLine4: String;

    @ApiProperty({
        description: 'addressLine5',
        example: ''

    })
    addressLine5: String;


    @ApiProperty({
        description: 'unitNo',
        example: ''

    })
    unitNo: String;

    @ApiProperty({
        description: 'postalCode',
        example: ''

    })
    postalCode: String;
}


export class OptionsDto {
    @ApiProperty({
        description: 'optId',
        example: 0
    })
    optId: number;

    @ApiProperty({
        description: 'optLabel',
        example: 'Spouse'

    })
    optLabel: String;

    @ApiProperty({
        description: 'optValue',
        example: 'SP'

    })
    optValue: String;

    @ApiProperty({
        description: 'optDesc',
        example: ''
    })
    optDesc: String;
}

export class InputDto {
    @ApiProperty({
        description: 'type',
        example: 'dropdown'

    })
    type: string;

    @ApiProperty({
        description: 'options'

    })
    option: OptionsDto;
}

export class RelationDto {
    @ApiProperty({
        description: 'input'
    })
    input: InputDto;
}

export class PolicyExtraInfoDto {
    @ApiProperty({
        description: 'activity category',
        example: 'highRisk'
    })
    activityCategory: string

    @ApiProperty({
        description: 'activity name',
        example: 'skiing'
    })
    activityName: string

    @ApiProperty({
        description: 'booking date',
        example: '2018-01-01 12:00:00'
    })
    bookingDate: string

    @ApiProperty({
        description: 'booking country',
        example: 'Singapore'
    })
    bookingCountry: string

    @ApiProperty({
        description: 'ticket start date',
        example: '2018-01-01 12:00:00'
    })
    ticketStartDate: string

    @ApiProperty({
        description: 'ticket end date',
        example: '2018-01-03 12:00:00'
    })
    ticketEndDate: string

    @ApiProperty({
        description: 'activity dtart date',
        example: '2018-01-01 12:00:00'
    })
    activityStartDate: string

    @ApiProperty({
        description: 'activity end date',
        example: '2018-01-03 12:00:00'
    })
    activityEndDate: string

    @ApiProperty({
        description: 'premium info',
        example: {
            retailPrice: "3",
            discountPrice: "2.7",
            fwdPrice: "1.5"
        }
    })
    premiumInfo: {
        retailPrice: number
        discountPrice: number
        fwdPrice: number
    }

    @ApiProperty({
        description: 'activity region',
        example: 'Central East'
    })
    activityRegion: string

    @ApiProperty({
        description: 'activity country',
        example: 'Singapore'
    })
    activityCountry: string
}


export class PolicyHolderExtraInfoDto {
    @ApiProperty({
        description: 'age declaration',
        example: 'Y'
    })
    ageConsentDeclaration: string

    @ApiProperty({
        description: 'postalCode',
        example: 'Y'
    })
    locationConsentDeclaration
}

export class PolicyHolderDto {
    @ApiProperty({
        description: 'isPolicyHolder',
        example: 'Y'

    })
    isPolicyHolder: String;

    @ApiProperty({
        description: 'firstName',
        example: ''

    })
    firstName: String;

    @ApiProperty({
        description: 'lastName',
        example: ''

    })
    lastName: String;

    @ApiProperty({
        description: 'title',
        example: ''

    })
    title: String;

    @ApiProperty({
        description: 'dob',
        example: '21-12-1996'

    })
    dob: string;

    @ApiProperty({
        description: 'gender',
        example: 'M'

    })
    gender: String;

    @ApiProperty({
        description: 'idCardNumber',
        example: 'PASSPORT'

    })
    idType: String;

    @ApiProperty({
        description: 'idTypeCode',
        example: 'T112341A'

    })
    idNumber: String;

    @ApiProperty({
        description: 'email',
        example: 'xxxx@gmail.com'

    })
    email: String;

    @ApiProperty({
        description: 'mobile',
        example: '12345678'

    })
    mobile: String;

    @ApiProperty({
        description: 'mobile',
        example: '65'

    })
    mobileCountryCode: string

    @ApiProperty({
        description: 'nationality',
        example: ''

    })
    nationality: String;

    @ApiProperty({
        description: 'smoker',
        example: ''

    })
    smoker: String;

    @ApiProperty({
        description: 'doc',
        example: []

    })
    supportingDocs: string[];

    // @ApiProperty({
    //     description: 'id'

    // })
    // id: IdDto;

    @ApiProperty({
        description: 'address',
        type: [AddressDto],
        minimum: 1,
        maximum: 1
    })
    address: [AddressDto];

    @ApiProperty({
        description: 'extraInfo',
        example: ''
    })
    extraInfo: PolicyHolderExtraInfoDto;

    @ApiProperty({
        description: 'relation'

    })
    relation: RelationDto;

    @ApiProperty({
        description: 'uw',
        type: [UwDto]

    })
    uw: [UwDto];


}

export class IdInsuredDto {
    @ApiProperty({
        description: 'idType',
        example: 'PASSPORT'

    })
    idType: String;

    @ApiProperty({
        description: 'idNo',
        example: '23456'
    })
    idNo: String;
}


export class InsuredDto {
    @ApiProperty({
        description: 'isPolicyHolder',
        example: 'Y'

    })
    isPolicyHolder: String;

    @ApiProperty({
        description: 'firstName',
        example: 'test'
    })
    firstName: String;

    @ApiProperty({
        description: 'lastName',
        example: 'test'
    })
    lastName: String;

    @ApiProperty({
        description: 'title',
        example: ''

    })
    title: String;

    @ApiProperty({
        description: 'title',
        example: 'test'
    })

    @ApiProperty({
        description: 'dob',
        example: "21-12-1996"

    })
    dob: string;

    @ApiProperty({
        description: 'gender',
        example: "M"

    })
    gender: String;

    @ApiProperty({
        description: 'idCardNumber',
        example: 'PASSPORT'

    })
    idType: String;

    @ApiProperty({
        description: 'idTypeCode',
        example: 'T112341A'

    })
    idNumber: String;

    @ApiProperty({
        description: 'email',
        example: 'xxxx@gmail.com'

    })
    email: String;

    @ApiProperty({
        description: 'mobile',
        example: '9121345'
    })
    mobile: String;

    @ApiProperty({
        description: 'nationality',
        example: 'Singapore'

    })
    nationality: String;

    @ApiProperty({
        description: 'smoker',
        example: 'N'

    })
    smoker: String;

    @ApiProperty({
        description: 'doc',
        example: []

    })
    supportingDocs: string[];

    @ApiProperty({
        description: 'address',
        type: [AddressDto],
        minimum: 1,
        maximum: 1

    })
    address: [AddressDto];

    @ApiProperty({
        description: 'extraInfo',
        example: {}
    })
    extraInfo: Object;

    @ApiProperty({
        description: 'relation'

    })
    relation: RelationDto;

    @ApiProperty({
        description: 'uw',
        type: [UwDto]
    })
    uw: [UwDto];
}