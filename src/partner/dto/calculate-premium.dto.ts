import { ApiProperty } from '@nestjs/swagger';
import {
    ApiResponsesDto, ProductItemDto, OthersDto, discountsItemDto, PremiumsItemDto, DeclarationDto
    , PolicyHolderDto, InsuredDto, RelationDto, PolicyExtraInfoDto
} from './common.dto';


import { calculatePremiumDataReqExample, calculatePremiumDataResExample } from '../../examples/calculate-premium'

export class RolesItemDto {
    @ApiProperty({
        description: 'insured',
        type: [InsuredDto]
    })
    insured: [InsuredDto];
}

export class CalculatePremiumDataReqDto {
    @ApiProperty({
        description: 'product'

    })
    product: ProductItemDto;

    @ApiProperty({
        description: 'roles'

    })
    roles: RolesItemDto;

    @ApiProperty({
        description: 'discounts'

    })
    discounts: discountsItemDto;

    @ApiProperty({
        description: 'others'

    })
    others: OthersDto;

}


export class CalculatePremiumReqDto {
    @ApiProperty({
        description: 'request calculate premium data',
        example: calculatePremiumDataReqExample
    })
    data: CalculatePremiumDataReqDto;
}


export class CalculatePremiumDataResDto {
    @ApiProperty({
        description: 'product'
    })
    product: ProductItemDto;

    @ApiProperty({
        description: 'roles'
    })
    roles: RolesItemDto;

    @ApiProperty({
        description: 'discounts'
    })
    discounts: discountsItemDto;

    @ApiProperty({
        description: 'extra info'
    })
    extraInfo: PolicyExtraInfoDto;

    @ApiProperty({
        description: 'premiums'
    })
    premiums: PremiumsItemDto;
}


export class CalculatePremiumResDto extends ApiResponsesDto {
    @ApiProperty({
        description: 'Response Data',
        example: calculatePremiumDataResExample
    })
    data: CalculatePremiumDataResDto;
}