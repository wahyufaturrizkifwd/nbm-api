import { ApiProperty } from '@nestjs/swagger';
import { ApiResponsesDto } from './common.dto';
import { cancelPoliciesReqExample, cancelPoliciesDataResExample } from '../../examples/cancel-policies'

export class CancelPoliciesReqDto {
    @ApiProperty({
        description: 'application Id',
        example: 'S32423423G'
    })
    applicationId: string

    @ApiProperty({
        description: 'policy Id',
        example: 'MPA00001'
    })
    policyId: string

    @ApiProperty({
        description: 'cancellation date',
        example: '2021-09-08'
    })
    cancellationDate: string

    @ApiProperty({
        description: 'cancellation reason',
        example: 'reason to cancel'
    })
    cancellationReason: string
}


export class cancelPoliciesDto {
    @ApiProperty({
        description: 'application Id',
        example: 'S32423423G'
    })
    applicationNum: string

    @ApiProperty({
        description: 'policy Id',
        example: 'MPA00001'
    })
    policyId: string

    @ApiProperty({
        description: 'cancellation reason',
        example: 'fail'
    })
    cancelStatus: string

    @ApiProperty({
        description: 'cancellation reason',
        example: 404
    })
    statusCode: number

    @ApiProperty({
        description: 'cancellation date',
        example: '2021-09-08'
    })
    cancelEffectiveDate: string
}


export class CancelPoliciesResDto extends ApiResponsesDto {
    @ApiProperty({
        description: 'Response Data',
        example: cancelPoliciesDataResExample
    })
    data: cancelPoliciesDto
}
