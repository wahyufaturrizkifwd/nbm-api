import { ApiProperty } from '@nestjs/swagger';
import {
    ApiResponsesDto, ProductItemDto, OthersDto, discountsItemDto, PremiumsItemDto, DeclarationDto
    , PolicyHolderDto, InsuredDto, RelationDto, PolicyExtraInfoDto
} from './common.dto';

import { policyListExample, submitAppliactionDataResExample } from '../../examples/submit-application'

export class OthersDataDto {
    @ApiProperty({
        description: 'beneficiary',
        example: ''
    })
    type: String;

    @ApiProperty({
        description: 'firstName',
        example: ''

    })
    firstName: String;

    @ApiProperty({
        description: 'lastName',
        example: ''

    })
    lastName: String;

    @ApiProperty({
        description: 'extraInfo',
        example: ''

    })
    extraInfo: String;

    @ApiProperty({
        description: 'proportion',
        example: '100%'

    })
    proportion: String;

    @ApiProperty({
        description: 'relation'
    })
    relation: RelationDto;
}

export class ReqRolesItemDto {
    @ApiProperty({
        description: 'policyHolder',

    })
    policyHolder: PolicyHolderDto;

    @ApiProperty({
        description: 'insured',
        type: [InsuredDto]

    })
    insured: [InsuredDto];

    @ApiProperty({
        description: 'others',
        type: [OthersDataDto]

    })
    others: [OthersDataDto];
}



export class SubmitAppliactionReqDataDto {
    @ApiProperty({
        description: 'applicationId',
        example: 'R07Y307701I9'

    })
    applicationId: String;

    @ApiProperty({
        description: 'policy Id',
        example: 'MPA00001'
    })
    policyId: string

    @ApiProperty({
        description: 'product'

    })
    product: ProductItemDto;

    @ApiProperty({
        description: 'roles'

    })
    roles: ReqRolesItemDto;

    @ApiProperty({
        description: 'declaration',
        type: [DeclarationDto],
        minimum: 1,
        maximum: 1
    })
    declaration: [DeclarationDto];

    @ApiProperty({
        description: 'premiums'

    })
    premiums: PremiumsItemDto;

    @ApiProperty({
        description: 'discounts'

    })
    discounts: discountsItemDto;

    @ApiProperty({
        description: 'extra info'
    })
    extraInfo: PolicyExtraInfoDto

    @ApiProperty({
        description: 'others'
    })
    others: OthersDto;
}

export class SubmitAppliactionReqDto {
    @ApiProperty({
        description: 'request submit application data',
        example: 1
    })
    policyTotal: number

    @ApiProperty({
        description: 'request submit application data',
        type: [SubmitAppliactionReqDataDto],
        example: policyListExample
    })
    policyList: [SubmitAppliactionReqDataDto];
}



export class SubmitAppliactionResDataDto {
    @ApiProperty({
        description: 'applicationId',
        example: 'R07Y307701I9'
    })
    applicationId: String;

    @ApiProperty({
        description: 'policyId',
        example: 'MPA00001'
    })
    policyId: String;

    @ApiProperty({
        description: 'product',
    })
    product: ProductItemDto;
}


export class SubmitAppliactionResDto extends ApiResponsesDto {
    @ApiProperty({
        description: 'Response Data',
        example: submitAppliactionDataResExample
    })
    data: SubmitAppliactionResDataDto;
}
