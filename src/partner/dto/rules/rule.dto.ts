import { ApiProperty } from '@nestjs/swagger';

export class AgeRulesDto {
    @ApiProperty({
        description: 'Age range From',
        example: 15
    })
    ageRangeFrom: string;

    @ApiProperty({
        description: 'Age range from unit',
        example: 'year'
    })
    ageRangeFromUnit: string;

    @ApiProperty({
        description: 'Age range to',
        example: 75
    })
    ageRangeTo: string;

    @ApiProperty({
        description: 'Age range to unit',
        example: 'year'
    })
    ageRangeToUnit: string;
}

export class InsuredListRulesDto {
    @ApiProperty({
        description: 'Max insured list',
        example: 'unlimited'
    })
    maxInsuredList: string
}

export class CoverageDurationRulesDto {
    @ApiProperty({
        description: 'Duration unit',
        example: 'day'
    })
    unit: string

    @ApiProperty({
        description: 'minDuration unit',
        example: 1
    })
    minDuration: string

    @ApiProperty({
        description: 'maxDuration unit',
        example: 365
    })
    maxDuration: string


}