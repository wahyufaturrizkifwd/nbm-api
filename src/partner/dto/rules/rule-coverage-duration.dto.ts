import { ApiProperty } from '@nestjs/swagger';
import { CoverageDurationRulesDto } from './rule.dto';

export class RuleCoverageDurationDto {
    @ApiProperty({
        description: 'Rule type',
        example: 'coverageDuration'
    })
    type: string;

    @ApiProperty({
        description: 'Rules'
    })
    rules: CoverageDurationRulesDto;
}

