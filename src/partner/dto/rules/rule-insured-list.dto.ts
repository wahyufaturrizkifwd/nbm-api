import { ApiProperty } from '@nestjs/swagger';
import { InsuredListRulesDto } from './rule.dto';

export class RuleInsuredListDto {
    @ApiProperty({
        description: 'Rule type',
        example: 'insuredList'
    })
    type: string;

    @ApiProperty({
        description: 'Rules'
    })
    rules: InsuredListRulesDto;
}

