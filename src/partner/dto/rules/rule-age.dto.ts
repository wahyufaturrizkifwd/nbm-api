import { ApiProperty } from '@nestjs/swagger';
import { AgeRulesDto } from './rule.dto';

export class RuleAgeDto {
    @ApiProperty({
        description: 'age type',
        example: 'age'
    })
    type: string;

    @ApiProperty({
        description: 'Rules'
    })
    rules: AgeRulesDto;
}


