import { ApiProperty } from '@nestjs/swagger';
import { ApiResponsesDto } from './common.dto';
import { productsDataResExample } from '../../examples/products'


export class ProductDataDto {
    @ApiProperty({
        description: 'product code',
        example: 'mpa'
    })
    productCode: String

    @ApiProperty({
        description: 'product name',
        example: 'Micro PA'
    })
    productName: String

    @ApiProperty({
        description: 'product name',
        example: 'micro pa is a personal insurance policy'
    })
    productDescription: string
}


export class ProductsDto extends ApiResponsesDto {
    @ApiProperty({
        description: 'Response Data',
        type: [ProductDataDto],
        example: productsDataResExample
    })
    data: [ProductDataDto]
}
