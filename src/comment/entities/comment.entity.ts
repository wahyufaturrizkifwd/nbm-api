import { Job } from 'src/jobs/entities/job.entity';
import { User } from 'src/users/entities/user.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinTable,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { CreateCommentRo } from '../dto/create-comment.dto';

@Entity('comment')
export class Comment {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('text')
  comment: string;

  @ManyToOne((type) => User)
  @JoinTable()
  user: User;

  @ManyToOne((type) => Job, (job) => job.comments)
  job: Job;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;

  toResponseObject(): CreateCommentRo {
    const { id, comment, createdAt, updatedAt, deletedAt } = this;

    const responseObject: any = {
      id,
      comment,
      createdAt,
      updatedAt,
      deletedAt,
    };

    return responseObject;
  }
}
