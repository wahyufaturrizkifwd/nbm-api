import 'dotenv/config';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Logger } from '@nestjs/common';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
const { createMockMiddleware } = require('openapi-mock-express-middleware');

const port = process.env.PORT;

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const config = new DocumentBuilder()
    .setTitle('The partner API docs')
    .setDescription('List of partner API with examples')
    .setVersion('1.0')
    .addApiKey({
      type: 'apiKey',
      description: 'API key to be aunthenticated',
      in: 'header',
    }, 'apiKey')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);

  app.use(
    '/mock',
    createMockMiddleware({
      spec: document,
      locale: 'en', // json-schema-faker locale, default to 'en'
      options: { // json-schema-faker options
        alwaysFakeOptionals: false,
        useExamplesValue: true,
        ignoreMissingRefs: true
        // maxItems: 1,
        // minItems: 1
        // ...
      },
      jsfCallback: (jsf, faker) => {
      }
    }),
  );

  await app.listen(port);
  Logger.log('Server running on http://localhost:' + port, 'Server Wahyu')
}
bootstrap();
