export interface Job {
  id: string;
  name: string;
  desc: string;
  isActive: boolean;
  createdAt: Date;
  updatedAt: Date;
  deletedAt: Date;
}
