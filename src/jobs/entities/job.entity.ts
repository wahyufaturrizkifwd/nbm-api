import { IsBoolean } from 'class-validator';
import { Comment } from 'src/comment/entities/comment.entity';
import { User } from 'src/users/entities/user.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { CreateJobRo } from '../dto/create-job.dto';

@Entity()
export class Job {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ type: 'text', unique: true })
  name: string;

  @Column('text')
  desc: string;

  @Column()
  @IsBoolean()
  isActive: boolean;

  @ManyToOne((type) => User, (user) => user.jobs)
  user: User;

  @OneToMany((type) => Comment, (comments) => comments.job, { cascade: true })
  comments: Comment[];

  @ManyToMany((type) => User, { cascade: true })
  @JoinTable()
  upvotes: User[];

  @ManyToMany((type) => User, { cascade: true })
  @JoinTable()
  downvotes: User[];

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;

  toResponseObject(): CreateJobRo {
    const { id, name, desc, isActive, createdAt, updatedAt, deletedAt } = this;

    const responseObject: any = {
      id,
      name,
      desc,
      isActive,
      createdAt,
      updatedAt,
      deletedAt,
    };

    return responseObject;
  }
}
