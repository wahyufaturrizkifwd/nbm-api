import { IsNotEmpty } from 'class-validator';
import { LoginUserRo } from 'src/users/dto/login-user.dto.';

export class CreateJobDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  desc: string;

  @IsNotEmpty()
  isActive: boolean;
}

export class CreateJobRo {
  id?: string;
  name?: string;
  desc?: string;
  isActive?: boolean;
  upvotes?: number;
  downvotes?: number;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
  user?: LoginUserRo;
}
