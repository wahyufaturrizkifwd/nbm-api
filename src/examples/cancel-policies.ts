import { CancelPoliciesReqDto, cancelPoliciesDto } from '../partner/dto/cancel-policies.dto'

export const cancelPoliciesReqExample: CancelPoliciesReqDto = {
    "applicationId": "S32423423G",
    "policyId": "MPA00001",
    "cancellationDate": "2021-09-08",
    "cancellationReason": ""
}

export const cancelPoliciesDataResExample: cancelPoliciesDto = {
    "applicationNum": "S32423423G",
    "policyId": "MPA00001",
    "cancelStatus": "fail",
    "statusCode": 404,
    "cancelEffectiveDate": ""
}