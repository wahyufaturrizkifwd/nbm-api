import { SubmitAppliactionReqDataDto, SubmitAppliactionResDataDto } from '../partner/dto/submit-applications.dto'

export const policyListExample: [SubmitAppliactionReqDataDto] =
    [
        {
            "applicationId": "R07Y307701I9",
            "policyId": "HL002",
            "product": {
                "productCode": "146799",
                "plan": {
                    "planCode": "mpaNormal",
                    "riders": [],
                    "coverage": {
                        "type": "day",
                        "sumAssured": "60000",
                        "coverageStart": "2018-01-01 00:00:00",
                        "coverageEnd": "2019-01-01 00:00:00"
                    }
                }
            },
            "roles": {
                "policyHolder": {
                    "firstName": "Jimmy",
                    "lastName": "Kwok",
                    "title": "",
                    "dob": "dd-mm-yyyy",
                    "gender": "M",
                    "email": "jimmy.kwok@fwd.com",
                    "mobile": "12345678",
                    "mobileCountryCode": "65",
                    "nationality": "SGP",
                    "smoker": "N",
                    "idType": "PASSPORT",
                    "idNumber": "T1231SA",
                    "supportingDocs": [],
                    "address": [
                        {
                            "addressType": "personal",
                            "addressStyle": "line",
                            "addressLine1": "...",
                            "addressLine2": "...",
                            "addressLine3": "...",
                            "addressLine4": "...",
                            "addressLine5": "...",
                            "unitNo": "...",
                            "postalCode": "..."
                        }
                    ],
                    "extraInfo": {
                        "ageConsentDeclaration": 'Y',
                        "locationConsentDeclaration": 'Y'
                    },
                    "isPolicyHolder": "Y",
                    "relation": {
                        "input": {
                            "type": "dropdown",
                            "option": {
                                "optId": 0,
                                "optLabel": "spouse",
                                "optValue": "SP",
                                "optDesc": ""
                            }
                        }
                    },
                    "uw": [
                        {
                            "uwQnId": "uwQn1",
                            "uwQnLabel": "Are you smoking",
                            "uwQnValue": true,
                            "uwQnAns": "Yes"
                        }
                    ]
                },
                "insured": [
                    {
                        "firstName": "John",
                        "lastName": "Doe",
                        "title": "",
                        "dob": "25-12-1991",
                        "gender": "M",
                        "email": "xxxx@gmail.com",
                        "mobile": "",
                        "nationality": "SGP",
                        "smoker": "N",
                        "idType": "PASSPORT",
                        "idNumber": "T1231SA",
                        "supportingDocs": [],
                        "address": [
                            {
                                "addressType": "personal",
                                "addressStyle": "line",
                                "addressLine1": "...",
                                "addressLine2": "...",
                                "addressLine3": "...",
                                "addressLine4": "...",
                                "addressLine5": "...",
                                "unitNo": "...",
                                "postalCode": "..."
                            }
                        ],
                        "extraInfo": {},
                        "isPolicyHolder": "Y",
                        "relation": {
                            "input": {
                                "type": "dropdown",
                                "option": {
                                    "optId": 0,
                                    "optLabel": "spouse",
                                    "optValue": "SP",
                                    "optDesc": ""
                                }
                            }
                        },
                        "uw": [
                            {
                                "uwQnId": "uwQn1",
                                "uwQnLabel": "Are you smoking",
                                "uwQnValue": true,
                                "uwQnAns": "Yes"
                            }
                        ]
                    }
                ],
                "others": [
                    {
                        "type": "beneficiary",
                        "firstName": "",
                        "lastName": "",
                        "extraInfo": "",
                        "proportion": "100%",
                        "relation": {
                            "input": {
                                "type": "dropdown",
                                "option": {
                                    "optId": 0,
                                    "optLabel": "spouse",
                                    "optValue": "SP",
                                    "optDesc": ""
                                }
                            }
                        }
                    }
                ]
            },
            "declaration": [
                {
                    "declarationQnId": "decQn1",
                    "declarationQnLabel": "Declaration 1",
                    "declarationQnValue": true,
                    "declarationQnAns": "Yes"
                }
            ],
            "premiums": {
                "currency": "SGD",
                "tax": "0.07",
                "beforeDiscount": {
                    "beforeTax": {
                        "premium": 1193
                    },
                    "afterTax": {
                        "premium": 1193
                    }
                },
                "afterDiscount": {
                    "beforeTax": {
                        "premium": 1093
                    },
                    "afterTax": {
                        "premium": 1093
                    }
                }
            },
            "discounts": {
                "product": "mpa",
                "promoCode": "PR0000",
                "promoCodeAmount": "AMT000",
                "promoCodeTierType": "Marketing Promo",
                "frontendPromoCode": "THVO2",
                "lifePromoCode": null,
                "lifePromoCodeAmount": null,
                "promoCodeDesc": null,
                "promoPercentValue": 0,
                "promoAmountValue": 0,
                "allPromoSame": true,
                "layeredPromoCode": {
                    "T1": [
                        {
                            "frontendCode": "THVO2",
                            "frontendLabel": "Enjoy THVO2",
                            "discountTierType": "Marketing Promo",
                            "discountType": "PERCENTAGE",
                            "discountValue": 0,
                            "voucherValue": 2000,
                            "voucherLabel": "starbucks voucher",
                            "genericVoucher": false,
                            "corePromoValue": 0,
                            "corePromoCode": "PR0000",
                            "additionalVoucherDiscount": [],
                            "finalPromoHash": null,
                            "finalPromoRand": null
                        }
                    ],
                    "T2": [
                        {
                            "frontendCode": "THVO2",
                            "frontendLabel": "Enjoy THVO2",
                            "discountTierType": "Marketing Promo",
                            "discountType": "PERCENTAGE",
                            "discountValue": 0,
                            "voucherValue": 2000,
                            "voucherLabel": "starbucks voucher",
                            "genericVoucher": false,
                            "corePromoValue": 0,
                            "corePromoCode": "PR0000",
                            "additionalVoucherDiscount": [],
                            "finalPromoHash": null,
                            "finalPromoRand": null
                        }
                    ],
                    "T3": [
                        {
                            "frontendCode": "THVO2",
                            "frontendLabel": "Enjoy THVO2",
                            "discountTierType": "Marketing Promo",
                            "discountType": "PERCENTAGE",
                            "discountValue": 0,
                            "voucherValue": 2000,
                            "voucherLabel": "starbucks voucher",
                            "genericVoucher": false,
                            "corePromoValue": 0,
                            "corePromoCode": "PR0000",
                            "additionalVoucherDiscount": [],
                            "finalPromoHash": null,
                            "finalPromoRand": null
                        }
                    ]
                },
                "promoCodeHash": "cc8e0cb5a6923168c6d2b0f3f27a3b1537305af303b67a333dc461d4db22da51",
                "promoCodeRand": "c701bf4c74f5722ff30bbb9235ffe7d2",
                "startDate": "2021-07-03 00:00:00",
                "expiryDate": "2022-08-03 23:59:00",
                "additionalQuestions": []
            },
            "extraInfo": {
                "activityCategory": "highRisk",
                "activityName": "Skiing",
                "bookingDate": "2018-01-01 12:00:00",
                "bookingCountry": "Singapore",
                "ticketStartDate": "2018-01-01 12:00:00",
                "ticketEndDate": "2018-01-02 12:00:00",
                "activityStartDate": "2018-01-01 12:00:00",
                "activityEndDate": "2018-01-02 12:00:00",
                "premiumInfo": {
                    "retailPrice": 3,
                    "discountPrice": 2.7,
                    "fwdPrice": 1.5
                },
                "activityRegion": "Central East",
                "activityCountry": "Singapore"
            },
            "others": {
                "utmCampaign": "",
                "utmContent": "",
                "utmMedium": "",
                "utmSource": "",
                "utmTerm": "",
                "partnerTxId": "",
                "partnerCode": ""
            }
        }
    ]


export const submitAppliactionDataResExample: SubmitAppliactionResDataDto = {
    "applicationId": "R07Y307701I9",
    "policyId": "",
    "product": {
        "productCode": "mpa",
        "plan": {
            "planCode": "mpaNormal",
            "riders": [],
            "coverage": {
                "sumAssured": "60,000",
                "coverageStart": "011221",
                "coverageEnd": "201221"
            }
        }
    }
}
