import { ProductDataDto } from '../partner/dto/products.dto'

export const productsDataResExample: ProductDataDto[] = [
    {
        "productCode": "mpa",
        "productName": "Micro PA",
        "productDescription": "micro pa is a personal insurance policy",
    },
    {
        "productCode": "ha",
        "productName": "Heart Attack",
        "productDescription": "Heart attack is another personal insurance policy",
    },
    {
        "productCode": "sa",
        "productName": "Stroke",
        "productDescription": "stroke is another personal insurance policy",
    }
]