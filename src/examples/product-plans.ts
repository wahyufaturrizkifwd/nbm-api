import { ProductPlansDataDto } from '../partner/dto/product-plans.dto'

export const productPlansDataResExample: ProductPlansDataDto = {
    "productCode": "mpa",
    "productName": "Micro PA",
    "productDescription": "",
    "plan": [
        {
            "planCode": "mpaNormal",
            "planName": "60K Plan - Normal Activities",
            "premiumType": "Single",
            "benefits": [],
            "riders": [],
            "currency": "SGD",
            "coverage": {
                "type": "day",
                "sumAssured": "60000",
                "coverageStart": "2018-01-01 00:00:00",
                "coverageEnd": "2019-01-01 00:00:00"
            },
            "validation": {
                "0": {
                    "type": "age",
                    "rules": {
                        "ageRangeFrom": "15",
                        "ageRangeFromUnit": "day",
                        "ageRangeTo": "75",
                        "ageRangeToUnit": "year"
                    }
                },
                "1": {
                    "type": "insured",
                    "rules": {
                        "maxInsuredList": "unlimited"
                    }
                },
                "2": {
                    "type": "coverageDuration",
                    "rules": {
                        "unit": "day",
                        "minDuration": "1",
                        "maxDuration": "365"
                    }
                }
            },
            "uw": [
                {
                    "uwQnId": "uwQn1",
                    "uwQnLabel": "Are you smoking",
                    "uwQnValue": true
                }
            ]
        }
    ],
    "declaration": [
        {
            "declarationQnId": "decQn1",
            "declarationQnLabel": "Declaration 1",
            "declarationQnValue": true
        }
    ]
}
